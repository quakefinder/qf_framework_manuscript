"""
File: supporting_roc_analysis.py

These are the subroutines for the ROC Analysis class.


Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import os
import numpy as np
import inspect

current_module_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

def calculate_skill_score(x_axis, y):
    """
    This is the area between the chance line for a MED or ROC.  It is the same
    as Zechars AreaSkillScore [ZJ08] but shifted by 0.5.

    I.e. A perfect ROC for Hattori is 0.5, and a perfect MED for Zechar is 1.
    Chance for Hattori is 0 and chance for Zechar is 0.5
    "worse than chance is -0.5 for Hattori and 0 for ZJ.

    We chose to follow Hattori's technique here.
    """
    n_events = y[-1]
    y_chance = get_chance_line(x_axis, n_events)
    dx = np.median(np.diff(x_axis))
    length_of_x_axis = len(x_axis)
    dx_vector = dx*np.ones(length_of_x_axis)
    skill_score = np.dot(y - y_chance, dx_vector)/(length_of_x_axis*n_events)
    return skill_score

def get_chance_line(x_axis, n_events):
    """
    Returns the chance line
    """
    n_cells = len(x_axis) - 1
    chance = [float(x)/(n_cells) * n_events for x in x_axis]
    return chance

def get_z_score(n_cells, n_events, skill_score):
    """
    Gets the z score based on the AUC for a given number of cells and events
    """
    n_non_events = n_cells - n_events
    standard_auc = standard_error_auc_roc(n_events, n_non_events)
    return skill_score/float(standard_auc)

def calculate_z_score_with_ties(n_cells, n_events, ties_info, skill_score):
    n_non_events = n_cells - n_events
    standard_auc_with_ties = standard_error_auc_roc_with_ties(n_events, n_non_events, ties_info)
    return skill_score/float(standard_auc_with_ties)

def standard_error_auc_roc(n_events, n_non_events):
    """
    following Mason & Graham (2002) Areas beneath the relative operating characterisitcs (ROC)
    and relative operating levels (ROL) curves: Statistical significance and interpretation
    """
    return np.sqrt((n_events + n_non_events + 1.0)/(12.0*n_events*n_non_events))

def standard_error_auc_roc_with_ties(n_events, n_non_events, ties_info):
    """
    This addresses the modification to the empirical stderr when
    rankings are tied.

    @type: ties_info: itereable of integers corresponding to how many elements in
    each tied group;  e.g. ties_info = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
    corresponds to 11 pairs of ties

    Added an additional correction factor of e*e' to pre-sqrt
    ties-correction.  Why?  Look at Mason & Graham Equation 12; Note that
    they have shwon that F and U can be approximated by N(mu, sigma**2)
    but the mu they reference is ee'/2 and NOT 1/2 ... i.e. the AUC is not normalized
    so the area of the plot is 1, rather it is normalized so the AUC is ee'.

    So we need to normalize the formula of Mason & Graham for this.  Clearly
    for the mean this is simply to divide by ee'.  This is equivalent (for
    our non-negative-definite valued distribution) to normalizing the random variable
    itself by ee'.  But what effect on the variance?  Note that the units of
    variance are those of the mean squared, where as the units of standard
    error are those of the mean.  If you can convince yourself that it is
    approprite to scale the random variable by ee' it follows from the definition
    of sigma and sigma**2 that the sigma will scale by the same factor (1/ee')
    (not sigma squared).  To do this we take sqrt of the expression for sigma**2
    in equation 12 (13) and THEN normalize by ee' (sqrt(ee')*sqrt(ee'))
    to get an updated expression for the standard error for the unit-area
    normalized data.

    This logic and Equations 12 and 13 should be sufficient to derive the formulae
    below.

    """
    total_cells = n_events + n_non_events
    no_ties_baseline = (total_cells + 1.0)/(12.0*n_events*n_non_events)
    correction_term = 0.0

    correction_scale = 1.0/((12.0 * total_cells) * (total_cells-1)*n_events*n_non_events)
    for n_ties in ties_info:
        correction_term += n_ties*(n_ties+1)*(n_ties-1)

    correction_term *= correction_scale

    std_err = np.sqrt(no_ties_baseline - correction_term)
    return std_err


def testing():
    """
    """
    e = 8; e_prime = 673
    print(standard_error_auc_roc(e, e_prime))
    ties_info = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
    print(standard_error_auc_roc_with_ties(e, e_prime, ties_info))
    z = get_z_score(60, 8, 0.1)

if __name__ == "__main__":
    testing()
