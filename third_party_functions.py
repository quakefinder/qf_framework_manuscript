"""
The functions found here were copied from:
https://github.com/pivotal-legacy/moves/blob/master/train-app/sliding_window.py
which is licensed under the Apache 2.0 License.

The original author appears to be John Vinyard, but his website, http://www.johnvinyard.com, is no longer
available.  A (slightly different) version of these functions, authored by him, appears in his zounds repository,
https://github.com/JohnVinyard/zounds
which is licensed under the MIT open source license, reproduced here:

The MIT License (MIT)
Copyright (c) 2019 John Vinyard

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import numpy as np
from numpy.lib.stride_tricks import as_strided as ast

def norm_shape(shape):
    """

    This method originally taken from http://www.johnvinyard.com/

    It is less a signal processing method and more a numpy-array-handling tool

    Normalize numpy array shapes so they're always expressed as a tuple,
    even for one-dimensional shapes.

    Parameters
        shape - an int, or a tuple of ints

    Returns
        a shape tuple
    """
    try:
        i = int(shape)
        return (i,)
    except TypeError:
        # shape was not a number
        pass
    try:
        t = tuple(shape)
        return t
    except TypeError:
        # shape was not iterable
        pass
    raise TypeError('shape must be an int, or a tuple of ints')

def sliding_window(a,ws,ss = None,flatten = True):
    """
    This method originally taken from http://www.johnvinyard.com/

    usage: sw = sliding_window(a,ws,ss = None,flatten = True):
    Return a sliding window over a in any number of dimensions

    Parameters:
        a  - an n-dimensional numpy array
        ws - an int (a is 1D) or tuple (a is 2D or greater) representing the size
             of each dimension of the window
        ss - an int (a is 1D) or tuple (a is 2D or greater) representing the
             amount to slide the window in each dimension. If not specified, it
             defaults to ws.
        flatten - if True, all slices are flattened, otherwise, there is an
                  extra dimension for each dimension of the input.

    Returns
        Array, if 2d dimensions are (num_windows, n_points_per_window)

        An array containing each n-dimensional window from a
        The Ensemblized time series seems in many ways like a MVTS, but it is NOT.
    It looks like one, because the data are a 2D array, with time moving along the
    "row" or zero axis, but the similarities stop there.
    The time axis is different for each row!  Each row spans the same duration,
    but the actual interval t0+row_duration is difference for each row,, i.e. the
    t0 is different for each row.

    In the case where the window length is 1, we obntain the special case where
    the ensemblized time series is just the transpose (not-conjugate) of the input
    time series.

    There is an excellent article on ensemblization in numpy
    http://www.johnvinyard.com/blog/?p=268
    """
    if None is ss:
        # ss was not provided. the windows will not overlap in any direction.
        ss = ws
    ws = norm_shape(ws)
    ss = norm_shape(ss)

    # convert ws, ss, and a.shape to numpy arrays so that we can do math in every
    # dimension at once.
    ws = np.array(ws)
    ss = np.array(ss)
    shape = np.array(a.shape)

    # ensure that ws, ss, and a.shape all have the same number of dimensions
    ls = [len(shape),len(ws),len(ss)]
    if len(set(ls)) != 1:
        raise ValueError(\
        'a.shape, ws and ss must all have the same length. They were %s' % str(ls))

    # ensure that ws is smaller than a in every dimension
    if np.any(ws > shape):
        raise ValueError(\
        'ws cannot be larger than a in any dimension.\
 a.shape was %s and ws was %s' % (str(a.shape),str(ws)))

    # how many slices will there be in each dimension?
    newshape = norm_shape(((shape - ws) // ss) + 1)
    # the shape of the strided array will be the number of slices in each dimension
    # plus the shape of the window (tuple addition)
    newshape += norm_shape(ws)
    # the strides tuple will be the array's strides multiplied by step size, plus
    # the array's strides (tuple addition)
    newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
    strided = ast(a,shape = newshape,strides = newstrides)
    if not flatten:
        return strided

    # Collapse strided so that it has one more dimension than the window.  I.e.,
    # the new array is a flat list of slices.
    meat = len(ws) if ws.shape else 0
    firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
    dim = np.array(firstdim + (newshape[-meat:]))
    # remove any dimensions with size 1
    dim = dim[dim != 1]
    return strided.reshape(dim)
