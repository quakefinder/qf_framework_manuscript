"""
File: roc_analysis.py

Produces the analysis information for a Receiver Operating Characteristic (ROC)
curve based on the total number of cells and the indices of flagged events.

Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from supporting_roc_analysis import get_z_score, calculate_skill_score, calculate_z_score_with_ties
from scipy.stats import norm
import numpy as np

class ROCAnalysis(object):
    """
    This only requires the number of cells (e.g. number of station-days) and
    the indices of the events.

    """
    def __init__(self, n_cells, indices_of_events, **kwargs):
        """
        """
        assert n_cells > 0, "Number of cells must be greater than 0"
        assert len(indices_of_events) > 0, "Event indices must be a list greater than 0"
        for event in indices_of_events:
            assert event >= 0, "Event index cannot be less than 0"
            assert event < n_cells, "Event index cannot be greater than number of cells"
        self._n_cells = n_cells
        self._indices_of_events = indices_of_events
        self._n_events = len(self._indices_of_events)
        self.n_quakes = None
        self._get_axes()
        self._calculate_skill_score()
        self.z_score = get_z_score(self._n_cells, self._n_events, self.skill_score)
        self.percentile = norm.cdf(self.z_score)

    def _get_axes(self):
        """
        Gets the x and y axes - makes the actual steps for the events
        """
        n_axes_cells = self._n_cells+1
        self._n_events = len(self._indices_of_events)
        self.x_axis = np.arange(n_axes_cells)
        self.y = np.zeros((n_axes_cells))
        ordered_event_indices = sorted(self._indices_of_events)
        for i_sample, idx in enumerate(ordered_event_indices):
            self.y[idx+1:] = i_sample + 1.0

    def _calculate_skill_score(self):
        """

        """
        self.skill_score = calculate_skill_score(self.x_axis, self.y)

    def get_z_score_with_ties(self, ties_info):
        """
        Calculates the z score with ties accounted for.
        @type: ties_info: itereable of integers corresponding to how many elements in
        each tied group;  e.g. ties_info = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        corresponds to 11 pairs of ties
        """
        return calculate_z_score_with_ties(self._n_cells, self._n_events, ties_info, self.skill_score)


    def hits_at(self, num_alarms):
        """
        Determines number of hits at a given alarm cell.
        This is the true positives
        """
        hits = self.y[num_alarms]
        return hits

    def hit_ratio(self, num_alarms):
        """
        Determines the ratio of hits to events at a given alarm cell
        """
        return (self.hits_at(num_alarms)/self._n_events)*100

    def misses_at(self, num_alarms):
        """
        Determines number of misses at a given alarm cell
        This is the false negatives
        """
        return self._n_events - self.hits_at(num_alarms)

    def true_negatives_at(self, num_alarms):
        """
        Determines number of true negatives at a given alarm cell
        """
        return self._n_cells - num_alarms - self.misses_at(num_alarms)

    def false_positives_at(self, num_alarms):
        """
        Determines number of false positives at a given alarm cell
        """
        return num_alarms - self.hits_at(num_alarms)

    def FAR(self, num_alarms):
        """
        Determines the ratio of false positives to number of alarmed cells
        at a given alarm cell
        """
        false_positives = self.false_positives_at(num_alarms)
        return (false_positives/num_alarms)*100

    def FNR(self, num_alarms):
        """
        Determines the ratio of misses to number of non-alarmed cells
        at a given alarm cell
        """
        negative_predicts = self._n_cells - num_alarms
        return (self.misses_at(num_alarms)/negative_predicts)*100

    def fp_to_hit_ratio(self, num_alarms):
        """
        Determines ratio of false positives to hits at a given alarm cell
        """
        return self.false_positives_at(num_alarms)/self.hits_at(num_alarms)

