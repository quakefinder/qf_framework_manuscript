"""
File: data_management.py

Loads the data as a numpy array. This will need to be filled in for the algorithm
to find the user's data.

Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np

MIN_DATA_COVERAGE = .99 * 4320000 #Minimum number of samples necessary for day of data

def load_data(station, data_date, component):
    """
    Loads the data for a given station-day and mag component. User will need to fill
    in this function for it to correctly locate the data.
    """
    if component == 'north':
        # Fill in data location
        filename = ""
    if component == 'east':
        # Fill in data location
        filename = ""
    if component == 'down':
        # Fill in data location
        filename = ""
    data = np.load(filename)
    if (data is None) or (len(data[np.isfinite(data)]) < MIN_DATA_COVERAGE):
        return None
    return data


