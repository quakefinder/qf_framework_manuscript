"""
File: roc_plot.py

Plots a Receiver Operating Characteristic (ROC) curve based on the total
number of cells and the indices of flagged events.

Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import os
import matplotlib.pyplot as plt
import numpy as np
from roc_analysis import ROCAnalysis
from supporting_roc_analysis import get_chance_line

HOME_DIRECTORY = os.path.expanduser("~/")

class ROCPlot(object):
    """
    This only requires the number of cells (e.g. number of station-days) and
    the indices of the events.
    """
    def __init__(self, n_cells, indices_of_events, **kwargs):
        """
        """
        self.output_dir = os.path.join(HOME_DIRECTORY, 'tmp/roc_plots')
        assert n_cells > 0, "Number of cells must be greater than 0"
        assert len(indices_of_events) > 0, "Event indices must be a list greater than 0"
        for event in indices_of_events:
            assert event >= 0, "Event index cannot be less than 0"
            assert event < n_cells, "Event index cannot be greater than number of cells"
        self._n_cells = n_cells
        self._indices_of_events = indices_of_events
        self.roc_analysis = ROCAnalysis(n_cells, indices_of_events)
        self._n_events = self.roc_analysis._n_events
        self.skill_score = self.roc_analysis.skill_score
        self.z_score = self.roc_analysis.z_score
        self.y = self.roc_analysis.y
        self.x_axis = self.roc_analysis.x_axis

    def get_skill_score_string(self):
        return 'AUC = {:.4f}'.format(self.skill_score)

    def get_z_score_string(self):
        return 'Z Score = {:.2f}'.format(self.z_score)

    def get_title(self):
        """
        Produces title
        """
        title = '{}, {}'.format(self.get_skill_score_string(), self.get_z_score_string())
        return title

    def _get_chance_line(self, x_axis):
        chance = get_chance_line(x_axis, self._n_events)
        return chance

    def _get_ROC_axes(self):
        """
        Gets the x and y axes - makes the actual steps for the events
        """
        n_axes_cells = (self._n_cells+1)- self._n_events
        x_axis = np.arange(n_axes_cells)
        y_axis = np.zeros((n_axes_cells))
        ordered_event_indices = sorted(self._indices_of_events)
        for i_sample, idx in enumerate(ordered_event_indices):
            y_index = idx - i_sample
            y_axis[y_index:] = i_sample + 1.0
        return x_axis, y_axis

    def plot_ROC(self, **kwargs):
        """
        Plots ROC diagram
        """
        save_plot = kwargs.get('save', True)
        show_plot = kwargs.get('show', True)
        plt.figure(1)
        plt.clf()
        ax = plt.axes()
        x_axis, y_axis = self._get_ROC_axes()
        chance_line = self._get_chance_line(x_axis)
        x_axis = x_axis/(1.0*self._n_cells-self._n_events)
        y_axis = y_axis/self._n_events
        chance_line = [x/float(self._n_events) for x in chance_line]
        ax.step(x_axis, y_axis, where='post', label='ROC curve', linewidth=3)
        ax.plot(x_axis, chance_line, label='Chance Line', linewidth=2)
        plt.legend(numpoints=1, loc=4)
        plt.xlabel("Fraction of False Positives (n'={})".format(self._n_cells-self._n_events), fontsize=18)
        plt.ylabel('Fraction of True Positives (n={})'.format(self._n_events), fontsize=18)
        plt.title(self.get_title(), fontsize=18)
        #<Saves and shows ROC diagram>
        if save_plot:
            if not os.path.isdir(self.output_dir):
                os.makedirs(self.output_dir)
            full_outfilename = os.path.join(self.output_dir, "ROC_plot.png")
            plt.savefig(full_outfilename+'.png',bbox_inches='tight')
        if show_plot:
            plt.tight_layout()
            plt.show()

def test_roc_plot():
    """
    testing the ROCPlot
    """
    n_cells = 800
    event_indices = [25,50,400,550,700]
    roc = ROCPlot(n_cells, event_indices)
    roc.plot_ROC(save=False)


if __name__ == "__main__":
    test_roc_plot()
