"""
File: core_algorithm_functions.py

Core functions used in the QF algorithm.


Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import datetime
import numpy as np
import pandas as pd
from supporting_algorithm_functions import compute_variance, compute_sta_lta, get_number_of_pulses
from data_management import load_data

def calculate_pulse_threshold(station_id, start_date, n_days_in_threshold):
    """
    Calculates the pulse threshold for a given station and date range. It will
    try to use the first 100-days of data and requires there be at least a minimum
    number of days (defined as min_finite_days) with data
    """
    min_finite_days = 98 #Minimum number of days of data required for threshold computation
    n_expected_samples = 113681 #Number of variance windows in a day
    # Get dates for threshold computation
    threshold_end_date = start_date + datetime.timedelta(days=n_days_in_threshold)
    list_of_dates = pd.date_range(start_date, threshold_end_date, closed='left')
    # Allocate memory
    big_sta_lta_array = np.full((len(list_of_dates), n_expected_samples), np.nan)
    for i_day, data_date in enumerate(list_of_dates):
        # Load data
        north = load_data(station_id, data_date, 'north')
        east = load_data(station_id, data_date, 'east')
        down = load_data(station_id, data_date, 'down')
        if (north is None) or (east is None) or (down is None):
            continue
        # Computes Total Field
        total_field = np.sqrt(north**2 + east**2 + down**2)
        # Calculates variance
        variance = compute_variance(total_field)
        # Calculates STA-LTA
        sta_lta = compute_sta_lta(variance)
        # Stores STA-LTA
        big_sta_lta_array[i_day,:] = sta_lta[:]
    # Check that there are enough days of data
    n_daily_finite_values = np.sum(np.isfinite(big_sta_lta_array), axis=1)
    n_days_with_data = len(np.where(n_daily_finite_values > 0)[0])
    if n_days_with_data < min_finite_days:
        print("Does not meet the minimum threshold ({}) of finite values.".format(min_finite_days))
        return None
    # Computes threshold which is the mean + standard devication
    big_sta_lta_array = big_sta_lta_array.reshape(-1)
    big_sta_lta_array = big_sta_lta_array[np.isfinite(big_sta_lta_array)]
    pulse_threshold = np.nanmean(big_sta_lta_array) + np.std(big_sta_lta_array)
    return pulse_threshold

def compute_pulses_per_day(north, east, down, pulse_threshold):
    """
    Computes the pulses-per-day from the north, east, down arrays using the pulse
    threshold supplied.
    """
    # Computes Total Field
    total_field = np.sqrt(north**2 + east**2 + down**2)
    # Calculates variance
    variance = compute_variance(total_field)
    # Calculates STA-LTA
    sta_lta = compute_sta_lta(variance)
    # Computes pulses-per-day
    pulses_per_day = get_number_of_pulses(sta_lta, pulse_threshold)
    return pulses_per_day

def get_station_day_rank(station_id, rank_date, pulse_threshold):
    """
    Computes the rank for a given station-day using the pulse threshold.
    """
    ranking_gap_window = 4 #Number of days to skip before applying the rank
    ranking_sta_window = 8 #Number of days in the ranking STA window
    ranking_lta_window = 100 #Number of days in the ranking LTA window
    min_finite_days = 98 #Minimum number of days of data required for ranking LTA window
    # Create date range needed for given station-day
    n_days_for_lta = ranking_lta_window + ranking_gap_window
    start_date_for_ranks = rank_date - datetime.timedelta(days=n_days_for_lta)
    end_date_for_ranks = rank_date - datetime.timedelta(days=ranking_gap_window)
    list_of_dates = pd.date_range(start_date_for_ranks, end_date_for_ranks, closed='left')
    # Allocate memory
    pulses_per_day = np.full(len(list_of_dates), np.nan)
    for i_day, data_date in enumerate(list_of_dates):
        # Load data for each component
        north_data = load_data(station_id, data_date, 'north')
        east_data = load_data(station_id, data_date, 'east')
        down_data = load_data(station_id, data_date, 'down')
        if (north_data is None) or (east_data is None) or (down_data is None):
            continue
        # Retrieve pulses-per-day
        pulses_per_day[i_day] = compute_pulses_per_day(north_data, east_data, down_data, pulse_threshold)
    # Check that there are enough days of data
    n_daily_finite_ranks = np.sum(np.isfinite(pulses_per_day))
    if n_daily_finite_ranks < min_finite_days:
        print("Does not meet the minimum number ({}) of finite values.".format(min_finite_days))
        return None
    # Compute normalized rank
    sta_start_day_index = ranking_lta_window - ranking_sta_window
    rank = np.nanmean(pulses_per_day[sta_start_day_index:])/np.nanmedian(pulses_per_day)
    return rank
