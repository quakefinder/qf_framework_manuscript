# QF_Framework_Manuscript

This is the reference implementation of the QuakeFinder algorithm as described in
"An Algorithmic Framework for Investigating the Temporal Relationship of
Magnetic Field Pulses and Earthquakes Applied to California."

This computes ranks for station-days, but it assumes that all QC was included
in the pre-processing of the data.

If you would like to find out more about this framework or algorithm, please
contact Daniel Schneider, <dschneider@quakefinder.com>.


