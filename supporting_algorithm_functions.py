"""
File: supporting_algorithm_functions.py

Supporting functions for the QF algorithm.


Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import numpy as np
from third_party_functions import sliding_window
from interval import Interval, merge_interval_list

WINDOW_LENGTH = 150 #Number of samples in a window
WINDOW_OVERLAP = 112 #Number of samples for which windows overlap
STA_DURATION = 3.0 #Seconds in the STA window
LTA_DURATION = 70.0 #Seconds in the LTA window
SAMPLES_PER_SECOND = 50.0

def compute_variance(time_series):
    """
    Calculates the variance time series
    """
    time_series = np.atleast_2d(time_series)
    windowed_array = sliding_window(time_series,(1, WINDOW_LENGTH), ss=(1, WINDOW_LENGTH-WINDOW_OVERLAP), flatten=True)
    variance_windows = np.var(windowed_array, axis=1)
    return variance_windows

def _compute_window_start_indices():
    """
    Computes the start indices of the sliding windows
    """
    time_series_length = SAMPLES_PER_SECOND * 86400
    window_advance = WINDOW_LENGTH - WINDOW_OVERLAP
    left_edge_indices = np.arange(0, time_series_length, window_advance)
    right_edge_indices = left_edge_indices + WINDOW_LENGTH
    left_edge_indices = left_edge_indices[right_edge_indices <= time_series_length]
    return left_edge_indices

def _get_num_taps_in_moving_average(duration):
    """
    Gets the number of samples in the moving average
    """
    advance_num_points = WINDOW_LENGTH - WINDOW_OVERLAP
    dt = 1./SAMPLES_PER_SECOND
    advance_duration = dt * advance_num_points
    window_duration = dt * WINDOW_LENGTH
    window_indices = _compute_window_start_indices()
    num_windows = len(window_indices)
    advances = advance_duration * np.arange(num_windows) + window_duration
    num_taps = np.argmin(np.abs(advances - duration)) + 1
    return num_taps

def compute_sta_lta(time_series):
    """
    Calculate the STA-LTA time series
    """
    n_moving_average_taps_sta = _get_num_taps_in_moving_average(STA_DURATION)
    moving_average_filter_sta = np.ones(n_moving_average_taps_sta)/n_moving_average_taps_sta
    n_moving_average_taps_lta = _get_num_taps_in_moving_average(LTA_DURATION)
    moving_average_filter_lta = np.ones(n_moving_average_taps_lta)/n_moving_average_taps_lta
    sta_time_series = time_series.copy()
    lta_time_series = time_series.copy()
    sta_time_series[sta_time_series==0] = np.mean(sta_time_series)
    sta_ts = np.convolve(sta_time_series, moving_average_filter_sta, 'same')
    lta_time_series[lta_time_series==0] = np.mean(lta_time_series)
    lta_ts = np.convolve(lta_time_series, moving_average_filter_lta, 'same')
    varat = sta_ts/lta_ts
    sta_lta_data = np.log10(varat)
    return sta_lta_data

def get_number_of_pulses(time_series, threshold):
    """
    Gets the number of pulses in the time series that exceed the threshold
    """
    window_indices = _compute_window_start_indices()
    dt = 1./SAMPLES_PER_SECOND
    window_duration = dt * WINDOW_LENGTH
    pulse_indices = np.where(time_series > threshold)[0]
    interval_list = []
    for ndx in pulse_indices:
        lower_bound_seconds = window_indices[ndx] * dt
        upper_bound_seconds = lower_bound_seconds + window_duration
        ivl = Interval(lower_bound=lower_bound_seconds,
                      upper_bound=upper_bound_seconds)
        interval_list.append(ivl)
    interval_list = merge_interval_list(interval_list)
    number_of_pulses = len(interval_list)
    return number_of_pulses

