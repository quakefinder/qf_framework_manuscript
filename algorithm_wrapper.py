"""
File: algorithm_wrapper.py

This is the reference implementation of the QuakeFinder algorithm as described in
"An Algorithmic Framework for Investigating the Temporal Relationship of
Magnetic Field Pulses and Earthquakes Applied to California."

This computes ranks for station-days, but it assumes that all QC was included
in the pre-processing of the data.

If you would like to find out more about this framework or algorithm, please
contact Daniel Schneider, <dschneider@quakefinder.com>.

To use this code:
User should fill in the location of the filename containing the station-intervals
information for the "station_intervals_filename" variable. This csv should contain
the information for each station's time interval with the following columns:
- "station_id" : station ID as an integer
- "start_date" : start date of the time interval as a datetime.datetime object
- "end_date" : end date of the time interval as a datetime.datetime object

User should fill in the location of the filename containing the qualifying earthquakes
information for the "qualifying_earthquakes_filename" variable. This csv will be used
for evaluation and should contain the following columns about the earthquakes:
- "station_id" : station ID as an integer
- "earthquake_date" : date of the earthquake as a datetime.datetime object


Copyright 2019 QuakeFinder, a division of Stellar Solutions, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import datetime
import numpy as np
import pandas as pd
from core_algorithm_functions import calculate_pulse_threshold, get_station_day_rank
from roc_plot import ROCPlot

# Fill in location of the CSV file containing station-intervals information
station_intervals_filename = ""

# Fill in location of the CSV file containing the qualifying earthquake information
qualifying_earthquakes_filename = ""

# ROC plot variables
SAVE_ROC_PLOT = True #To save the ROC plot
SHOW_ROC_PLOT = True #To show the ROC plot
ROC_PLOT_OUTPUT_DIR = "/tmp" #Directory of where to save the ROC plot

def main():
    """
    """
    # Read in the station-intervals information and qualifying earthquakes information
    station_interval_df = pd.read_csv(station_intervals_filename, parse_dates=['start_date', 'end_date'])
    qualifying_eq_df = pd.read_csv(qualifying_earthquakes_filename, parse_dates=['date'])

    n_days_in_threshold = 100 #Number of days to average together for pulse threshold
    station_intervals_ranks = pd.DataFrame()
    for i_stn_ivl, station_interval in station_interval_df.iterrows():
        # Gathers station-interval information
        station_id = station_interval['station_id']
        start_date = station_interval['start_date']
        end_date = station_interval['end_date']

        # Computes threshold used for identifying pulses
        pulse_threshold = calculate_pulse_threshold(station_id, start_date, n_days_in_threshold)
        if pulse_threshold is None:
            print("No pulse threshold computed for station {} using {} as start date.".format(
                    station_id, start_date))
            continue
        # Gets the start date for valid ranks
        rank_start_date = start_date + datetime.timedelta(days=n_days_in_threshold)
        list_of_rank_dates = pd.date_range(rank_start_date, end_date)
        # Computes ranks for the station-interval
        for i_date, rank_date in enumerate(list_of_rank_dates):
            rank = get_station_day_rank(station_id, rank_date, pulse_threshold)

            # Checks whether the station-day observed an earthquake
            qualifying_earthquakes = qualifying_eq_df[(qualifying_eq_df['station_id'] == station_id) &
                                                      (qualifying_eq_df['date'] == rank_date)]
            if qualifying_earthquakes.empty:
                earthquake_day = False
            else:
                earthquake_day = True
            # Stores rank and earthquake information for each station-day
            station_day_rank = pd.DataFrame({'date': rank_date,
                                             'station_id': station_id,
                                             'rank': rank,
                                             'earthquake': earthquake_day}, index=[0])
            station_intervals_ranks = pd.concat([station_intervals_ranks, station_day_rank])
    # Sorts the ranks and removes any non-finite valued ranks
    station_intervals_ranks = station_intervals_ranks.sort_values(['rank'], ascending=False)
    station_intervals_ranks = station_intervals_ranks[np.isfinite(station_intervals_ranks['rank'])].reset_index(drop=True)
    # Plots ROC curve
    event_indices = station_intervals_ranks[station_intervals_ranks['earthquake'] == True].index
    roc_plot = ROCPlot(len(station_intervals_ranks), event_indices)
    roc_plot.output_dir = ROC_PLOT_OUTPUT_DIR
    roc_plot.plot_ROC(save=SAVE_ROC_PLOT, show=SHOW_ROC_PLOT)


if __name__ == "__main__":
    main()

